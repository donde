class Util
  def self.build_uri_params( a)
    "?"+(a.map {|k,v| k.to_s+"="+v.to_s}).join("&")
  end
end