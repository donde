if __FILE__ == $0

require 'guiaoleo.rb'

# Toy variable to limit the number of pages so that we can test a bit
max_pages = 10

go = Guiaoleo.new
i = 0
restaurants = []
restaurants.concat(go.fetch_page(i+=1)) until go.pages != nil && (i >= go.pages || i >= max_pages)

puts "Finished fetching, now importing"
# NOTE: I had to move this require to after parsing is done, otherwise it was breaking the
# results of the regular expressoins, particularly trying to extract the address. Don't ask me why
require '../dondeback/config/environment'

restaurants.each do |restaurant|
  v = Venue.find_by_name(restaurant['name'])
  if !v.nil? and v.address == restaurant['address'] then
    puts "Skipping = [" + v.name + "]"
  else
    Venue.create(restaurant)
  end
end

puts "Done importing, now geocoding"
require 'geocode'

# Find all venues that lack coordinates and geocode each of them individually
# TODO Look for alternative geocoders, hopefully supporting batch geocoding
venues = Venue.find(:all,:conditions => ["lat is null"])
puts "There are " + venues.length.to_s + " unmapped venues"
venues.each {|v| Geocode.map_venue v}

end