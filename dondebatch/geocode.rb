require '../dondeback/config/environment'
require 'net/http'
require 'uri'
require 'xmlsimple'  
require 'util'

class Geocode
  @@G_URL = 'http://maps.google.com/maps/geo'
  @@G_KEY = 'ABQIAAAAaL54u5YxL6posXHh00u2PBT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQj9awGmpBO0LLwa-_2FLgcD0DZ2A'
  
  def self.map_venue( v)
    puts "Resolving: " + v.address
    res = Net::HTTP.get(URI.parse(URI.escape(@@G_URL+Util.build_uri_params({ 
      'q' => v.address,
      'sensor' => 'false',
      'output' => 'XML',
      'gl' => 'ar',
      'oe' => 'utf8'
    }))))
    data = XmlSimple::xml_in(res) unless res.nil?
    if res.nil? || data['Response'][0]['Status'][0]['code'][0].to_i != 200
      puts "Failed"
      return
    end
    puts "Resolved"
    coords = data['Response'][0]['Placemark'][0]['Point'][0]['coordinates'][0].split(",")
    v.long = coords[0]
    v.lat = coords[1]
    v.save
    puts "Saved"
  end
end # class