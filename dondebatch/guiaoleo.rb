require 'net/http'
require 'uri'
require 'iconv'

require 'util'

class Guiaoleo
  @@url = 'http://www.guiaoleo.com.ar/list.php'
  @@iconv = Iconv.new('UTF-8','ISO-8859-1')
  attr_reader :pages

  def fetch_page(page_num)
    puts 'Fetching page ' + page_num.to_s
    res = Net::HTTP.get(URI.parse(@@url+Util.build_uri_params({ 
      'Page' => page_num,
      'navby' => 'multiply',
      # For now, hardcode search to Capital only, since addresses returned in list don't include city
      'zona' => '10,15,31,5,30,19,2,22,32,13,18,7,20,3,23,55,6,1,24,25,4,8,17,12,16,9'
    })))
    res = @@iconv.iconv(res)

    # Parse the list of restaurants. Currently parsed elements:
    # - name, address, detail URL
    restaurants = []
    res.scan(/detail.php.*?\/tr/).each do |row|
      restaurant = {}
      row =~ /ID=(\d+)/
      restaurant['url'] = 'http://www.guiaoleo.com.ar/detail.php?ID=' + $~[1]
      row =~ />(.*?)</
      restaurant['name'] = $~[1]
      cols = row.scan(/<td>.*?<\/td>/)
      cols[2]  =~ /2>(.+?)</
      if $~.nil?
         puts "Failed parsing. COL = [" + cols[2] + "] from ROW = [" + row + "]"
      else
        restaurant['address'] = $~[1] + ", capital federal, argentina"
      end
      restaurants.push(restaurant)
    end
    
    # Parse the total number of pages
    @pages=res.scan(/Page=(\d+)/).inject(0) { |max,n| n[0].to_i>max ? n[0].to_i : max }
    
    return restaurants
  end
end