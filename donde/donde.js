//	We use this global variable to store re-usable objects
//	If this grows too big, it's time to re-think the approach
var _g = {};
_g['host_back'] = 'cubito.ath.cx:3000'
_g.retries = {};	//	Used to keep count of retries by address

function searchAndPin() {
	//	Get map bounds
	var bounds = _g.map.getBounds();
	var sw = bounds.getSouthWest();
	var ne = bounds.getNorthEast();
	document.getElementById("latlng").innerHTML=_g.map.getCenter();
	
	// Clear current Overlays
	_g.map.clearOverlays();
	
	//	Query restaurants in that area and puts a pin for each found
	get_resto_list({a0:sw.lat(),o0:sw.lng(),a1:ne.lat(),o1:ne.lng()},function(data) {
		var venues = data;
		for(var i=0;i<venues.length;i++) {
			var venue = venues[i];
			put_pin(venue.lat, venue["long"], venue.name);
		}
	});
}
function main(){
	if (!GBrowserIsCompatible()) {
		alert("Browser not compatible");
	}
	
	//	Create map and store as global
	_g.map = new GMap2(document.getElementById("map"));
	_g.map.addControl(new GLargeMapControl());
	
	//	Create goecoder and store as global
	_g.geocoder = new GClientGeocoder();
	_g.geocoder.setBaseCountryCode("ar");
	
	//	Position map arbitrarily in Nuñez
	/*  _g.geocoder.getLatLng("nuñez, capital federal, argentina",
	    function( pt) { 
		_g.map.setCenter( pt, 15);	
		});
	*/
	//	Position map arbitrarily in Nuñez without geocoding
	_g.map.setCenter(new GLatLng(-34.5585098, -58.4824486),15);
	
	// Searches current map position and puts pins
	searchAndPin();
	
	// Creates listener to check for map repositioning
	GEvent.addListener(_g.map,"moveend",function (){
		searchAndPin();
	});
}

/**
* Puts a pin in the map, given lat, lng and a name (from venues)
*/
function put_pin( lat, lng, name) {
       var marker = new GMarker(new GLatLng(lat, lng));
       _g.map.addOverlay( marker, {clickable: true});
       GEvent.addListener( marker, "click", function() {
                       marker.openInfoWindow(name);
       });
}

function get_resto_list( parameters, callback) {
	new Ajax.ScriptRequest( "http://" + _g['host_back'] + "/venues/viewport", {
		parameters: parameters,
		onResponse: callback,
		callbackName: "callback",
		onFailure: function( e) {
			alert("Error listing restaurants: "  + e);
		}
	});
}
