class CreateVenues < ActiveRecord::Migration
  def self.up
    create_table :venues do |t|
      t.decimal :lat, :long, :precision => 11, :scale => 8
      t.string :name, :address, :url
    end
  end

  def self.down
    drop_table :venues
  end
end
