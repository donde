class VenuesController < ApplicationController
  def index
    @venues = Venue.find(:all)
    
    respond_to do |format|
      format.json { render :json => {"venues" => @venues.to_json() }}
    end
  end
  
  def viewport
    return unless validate_params params, [:a0,:o0,:a1,:o1]
    
    @venues = Venue.find(:all,
      :conditions => ["lat > ? and `long` > ? and lat < ? and `long` < ?",
        params[:a0],params[:o0],params[:a1],params[:o1]])

    render :json => @venues.to_json(), :callback => params[:callback]
  end
  
private
  def validate_params(params, required_vars)
    required_vars.each do |param|
      if params[param].nil? then
        render :json => {"message" => "Parameter missing: " + param.to_s }, :status => 400
        return false
      end
    end
    return true
  end
end
